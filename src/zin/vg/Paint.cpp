#include <zin/vg/Paint.hpp>

Paint::Paint  () {
    m_Paint = vgCreatePaint();
}
Paint::~Paint () {
    // pure virtual destructor, nothing to do
}

void
Paint::Bind (const VGbitfield& paintMode) const {
    vgSetPaint(m_Paint, paintMode);
}


