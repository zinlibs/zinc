#include <zin/vg/Path.hpp>

#include <iostream>

/////
Path::Path () {
    m_Path = vgCreatePath(VG_PATH_FORMAT_STANDARD,  // path format
                        VG_PATH_DATATYPE_F,         // datatype
                        1,                          // scale
                        0,                          // bias,
                        0,                          // segCapacityHint
                        0,                          // coord capacity hint
                        VG_PATH_CAPABILITY_ALL );   // capabilities
//    m_Scale = sf::Vector2f(1.f,1.f);
    m_Fill   = NULL;
    m_Stroke = NULL;
}

/////
Path::~Path () {
    vgDestroyPath (m_Path);
}

/////
void
Path::SetStroke (const Paint& stroke) {
    m_Stroke = &stroke;
}

/////
void
Path::SetFill (const Paint& fill) {
    m_Fill = &fill;
}


/*
/////
void
Path::SetPosition (const sf::Vector2f& position) {
    m_Position = position;
}

/////
void
Path::SetRotation (const float& rotation) {
    m_Rotation = rotation;
}

/////
void
Path::SetScale(const sf::Vector2f& scale)
{
    m_Scale = scale;
}


/////
const sf::Vector2f &
Path::GetPosition () const {
    return m_Position;
}

/////
const float&
Path::GetRotation() const {
    return m_Rotation;
}

/////
const sf::Vector2f&
Path::GetScale () const {
    return m_Scale;
}
*/

/////
Path&
Path::LineTo (const sf::Vector2f& point, bool absolute) {
    VGubyte seg = VG_LINE_TO | absolute ? VG_ABSOLUTE : VG_RELATIVE;
    VGfloat data[2];

    data[0] = point.x; data[1] = point.y;
    vgAppendPathData(m_Path, 1, &seg, data);

    return *this;
}

/////
Path&
Path::HLineTo (const float& x, bool absolute) {
    VGubyte seg = VG_HLINE_TO | absolute ? VG_ABSOLUTE : VG_RELATIVE;
    VGfloat data = x;

    vgAppendPathData(m_Path, 1, &seg, &data);

    return *this;
}


////
Path&
Path::VLineTo (const float& y, bool absolute) {
    VGubyte seg = VG_VLINE_TO | absolute ? VG_ABSOLUTE : VG_RELATIVE;
    VGfloat data = y;

    vgAppendPathData(m_Path, 1, &seg, &data);

    return *this;
}


///
Path&
Path::QuadTo (const sf::Vector2f& p1, const sf::Vector2f& p2, bool absolute) {

    VGubyte seg = VG_QUAD_TO | absolute ? VG_ABSOLUTE : VG_RELATIVE;
    VGfloat data[4];

    data[0] = p1.x; data[1] = p1.y;
    data[2] = p2.x; data[3] = p2.y;

    vgAppendPathData(m_Path, 1, &seg, data);

    return *this;
}

///
Path&
Path::MoveTo (const sf::Vector2f& point, bool absolute) {

    VGubyte seg = VG_MOVE_TO | absolute ? VG_ABSOLUTE : VG_RELATIVE;
    VGfloat data[2];

    data[0] = point.x; data[1] = point.y;
    vgAppendPathData(m_Path, 1, &seg, data);

    return *this;
}

///
Path&
Path::Close () {

    VGubyte seg = VG_CLOSE_PATH;
    VGfloat data = 0.0f;

    vgAppendPathData(m_Path, 1, &seg, &data);

    return *this;
}

///
VGPath&
Path::GetNativePath () {
    return m_Path;
}

///
void
Path::Render        (sf::RenderTarget& target, sf::Renderer& renderer) const {

    vgSeti(VG_MATRIX_MODE, VG_MATRIX_PATH_USER_TO_SURFACE);
    vgLoadIdentity();
    vgTranslate(GetPosition().x, GetPosition().y);
    vgRotate(GetRotation());
    vgScale(GetScale().x, GetScale().y);


    // reset VG_PAINT status
    vgSetPaint(VG_INVALID_HANDLE, VG_STROKE_PATH);
    vgSetPaint(VG_INVALID_HANDLE, VG_FILL_PATH);

    // bind internal paint
    if(m_Stroke)
        m_Stroke->Bind(VG_STROKE_PATH);
    if(m_Fill)
        m_Fill->Bind(VG_FILL_PATH);

    // draw
    vgDrawPath(m_Path, VG_STROKE_PATH | VG_FILL_PATH);

}
