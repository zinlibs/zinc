#include <zin/vg/Color.hpp>


Color::Color (const sf::Uint8& r, const sf::Uint8& b, const sf::Uint8& g, const sf::Uint8& alpha) {
    m_Color.r = r;
    m_Color.b = b;
    m_Color.g = g;
    m_Color.a = alpha;

    vgSetParameteri(m_Paint,VG_PAINT_TYPE, VG_PAINT_COLOR);

    Update();
}

Color::Color (const sf::Color& color) {
    m_Color = color;

    Update();
}

Color::~Color () {
    vgDestroyPaint(m_Paint);
}

Color&
Color::operator= (const sf::Color& color) {
    m_Color = color;
    Update();
}

Color&
Color::operator= (const Color& color) {
    m_Color = color.GetColor();
    Update();
}

const sf::Color&
Color::GetColor () const {
    return m_Color;
}

void
Color::Update () {
    VGfloat col[] = { m_Color.r/255.f, m_Color.b/255.f, m_Color.g/255.f, m_Color.a/255.f};
    vgSetParameterfv(m_Paint, VG_PAINT_COLOR, 4, col);
}