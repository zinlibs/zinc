#include <zin/vg/Gradient.hpp>


Gradient::Gradient() :
    Paint() {
}

Gradient::~Gradient() {

}

void
Gradient::SetStop (const float& percent, const sf::Color& col) {
    m_Stops[percent] = col;
    const std::size_t size= m_Stops.size()*5;
    VGfloat data[size];

    std::map<float, sf::Color>::iterator it=m_Stops.begin();
    int i = 0;

    while(it != m_Stops.end()) {
        data[i]   = it->first;
        data[i+1] = it->second.r/255.f;
        data[i+2] = it->second.b/255.f;
        data[i+3] = it->second.g/255.f;
        data[i+4] = it->second.a/255.f;

        i+=5;
        it++;
    }


    vgSetParameterfv(m_Paint,VG_PAINT_COLOR_RAMP_STOPS, sizeof(data) , data);
}

void
Gradient::Clear () {
    m_Stops.clear();
    vgSetParameterfv(m_Paint,VG_PAINT_COLOR_RAMP_STOPS, sizeof(NULL), NULL);
}

void
Gradient::SetSpread (SpreadMethod method) {
    m_Spread = method;
    vgSetParameteri(m_Paint, VG_PAINT_COLOR_RAMP_SPREAD_MODE, method);
}



/////////////////////////



LinearGradient::LinearGradient (const sf::Vector2f& start, const sf::Vector2f& end) :
    Gradient(){
    m_Start = start;
    m_End = end;
    Update();
    vgSetParameteri(m_Paint, VG_PAINT_TYPE, VG_PAINT_TYPE_LINEAR_GRADIENT);
}

void
LinearGradient::SetStart (const sf::Vector2f& start) {
    m_Start = start;
    Update();
}

void
LinearGradient::SetEnd (const sf::Vector2f& end) {
    m_End = end;
    Update();
}

void
LinearGradient::Update () {
    VGfloat data[] = {m_Start.x, m_Start.y, m_End.x, m_End.y} ;
    vgSetParameterfv(m_Paint,VG_PAINT_LINEAR_GRADIENT, 4, data);
}
