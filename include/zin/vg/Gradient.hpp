#ifndef ZINC_GRADIENT_HPP
#define ZINC_GRADIENT_HPP

#include <zin/vg/Paint.hpp>
#include <SFML/Graphics/Color.hpp>
#include <SFML/System/Vector2.hpp>

#include <map>


///
/// \todo doc
///
class Gradient : public Paint {

public:

    enum SpreadMethod {
        Pad=VG_COLOR_RAMP_SPREAD_PAD,
        Reflect=VG_COLOR_RAMP_SPREAD_REFLECT,
        Repeat=VG_COLOR_RAMP_SPREAD_REPEAT
    };

    Gradient();

    virtual ~Gradient   ()=0; // virtual pure destructeur

    void
    SetStop             (const float& position, const sf::Color& col);

    void
    Clear               ();

    void
    SetSpread           (SpreadMethod method);



private:
    std::map<float, sf::Color> m_Stops; // auto sorting
    SpreadMethod m_Spread;
};



/// \todo doc
class LinearGradient : public Gradient {

public:
    LinearGradient      (const sf::Vector2f& start, const sf::Vector2f& end);

    void
    SetStart            (const sf::Vector2f& start);

    void
    SetEnd              (const sf::Vector2f& end);

    void
    Update              ();

private:
    sf::Vector2f        m_Start,
                        m_End;

};

#endif // ZINC_GRADIENT_HPP