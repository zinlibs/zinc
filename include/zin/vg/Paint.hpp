#ifndef ZINC_PAINT_HPP
#define ZINC_PAINT_HPP
#include <SFML/System/Resource.hpp>
#include <vg/openvg.h>

class Path;

class Paint : public sf::Resource<Paint> {

public:
    Paint();
    virtual ~Paint() =0;

protected:
    friend class Path;

    /////
    void
    Bind        (const VGbitfield& paintMode) const;

protected:
    VGPaint m_Paint;
};

#endif