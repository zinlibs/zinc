#ifndef ZINC_COLOR_HPP
#define ZINC_COLOR_HPP

#include <zin/vg/Paint.hpp>
#include <SFML/Graphics/Color.hpp>

class Color : public Paint {

public:

    ///
    /// \todo doc
    ///
    Color               (const sf::Uint8& r, const sf::Uint8& b, const sf::Uint8& g, const sf::Uint8& alpha);

    ///
    /// \todo doc
    ///
    Color               (const sf::Color& color);

    ///
    /// \todo doc
    ///
    ~Color();

    ///
    /// \todo doc
    ///
    Color&
    operator=           (const sf::Color& color);

    ///
    /// \todo doc
    ///
    Color&
    operator=           (const Color& color);

    ///
    /// \todo doc
    ///
    const sf::Color&
    GetColor            () const;

private:
    void
    Update              () ;

    sf::Color m_Color;
};

#endif // ZINC_COLOR_HPP


