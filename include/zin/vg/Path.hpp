#ifndef PATH_HPP
#define PATH_HPP

#include <vg/openvg.h>
#include <vg/vgu.h>
#include <SFML/System.hpp>
#include <SFML/Graphics/Drawable.hpp>
#include <zin/vg/Paint.hpp>
#include <SFML/System/Resource.hpp>

///
/// \brief manipulation de chemin
///
/// Manipulation de chemin bas niveau
/// Utiliser les classes Rect, Ellipse, Circle, Line
///

class Path : public sf::Drawable {

   public:
       ///
       /// \brief default constructor
       ///
       Path             ();

       ///
       /// \brief destruct the path
       ///
       ~Path            ();

       ///
       /// \todo doc
       ///
       void
       SetStroke        (const Paint& paint);

       ///
       /// \todo doc
       ///
       void
       SetFill          (const Paint& paint);
/*
       /// \todo : doc
       void
       SetPosition      ( const sf::Vector2f& position ) ;

       /// \todo : doc
       void
       SetRotation      ( const float& rotation );

       /// \todo : doc
       void
       SetScale         ( const sf::Vector2f& scale ) ;

       /// \todo : doc
       const sf::Vector2f&
       GetPosition      () const;

       /// \todo : doc
       const float&
       GetRotation      () const;

       /// \todo : doc
       const sf::Vector2f&
       GetScale         () const;
*/

       ///
       /// \brief line from last position to point
       /// \param point : the point to join
       /// \return the path
       ///
       Path&
       LineTo           (const sf::Vector2f& point, bool absolute=true);

       ///
       /// \brief horizontal line
       /// \param x : x offset of the line
       /// \param absolute : true = absolute and false = relative
       /// \return the path
       ///
       Path&
       HLineTo          (const float& x, bool absolute=true);

       ///
       /// \brief vertical line
       /// \param y : y offset
       /// \param absolute : true = absolute and false = relative
       /// \return the path
       ///
       Path&
       VLineTo          (const float& y, bool absolute=true);

       ///
       /// \brief make a quad
       /// \param p1 : 1st point
       /// \param p2 : x and y p1's opposed point
       /// \param absolute : true = absolute and false = relative
       ///
       Path&
       QuadTo           (const sf::Vector2f& p1, const sf::Vector2f& p2, bool absolute=true);

       ///
       /// \brief make a cube
       /// \param p1 : 1st point
       /// \param p2 : x or y p1's opposed point
       /// \param p3 : x and y p1's opposed point
       /// \param absolute : true = absolute and false = relative
       /// \return the path
       ///
       Path&
       CubicTo          (const sf::Vector2f& p1, const sf::Vector2f& p2, const sf::Vector2f& p3, bool absolute=true);

       ///
       /// \brief as QuadTo, but 1st point is current point
       /// \param p2 :TODO
       /// \param absolute : true = absolute and false = relative
       /// \return the path
       ///
       Path&
       QuadTo           (const sf::Vector2f& p2, bool absolute=true);

       ///
       /// \brief as CubicTo, but 1st point is current point
       /// \param p2 : TODO
       /// \param p3 : TODO
       /// \param absolute : true = absolute and false = relative
       /// \return the path
       ///
       Path&
       CubicTo          (const sf::Vector2f& p2, const sf::Vector2f& p3, bool absolute=true);

       ///
       /// \brief TODO
       /// \param absolute : true = absolute and false = relative
       /// \return the path
       ///
       Path&
       ArcTo            (const sf::Vector2f& pr /* ??? TODO */ , const sf::Vector2f& point, float rotation, bool absolute=true);

       ///
       /// \brief TODO
       /// \param absolute : true = absolute and false = relative
       /// \return the path
       /// \TODO make function ?
       ///
       //Path&
       //ArcTo .... as QuadTo and QuadTo :  TODO

       ///
       /// \brief move to point
       /// \param point : the new position
       /// \return the path
       ///
       Path&
       MoveTo           (const sf::Vector2f& point, bool absolute=true);


       ///
       /// \brief join the begining of the path with the end
       /// \TODO return void ?
       Path&
       Close            ();

       ///
       /// \brief return the internal path
       /// \return the internal path
       ///
       VGPath&
       GetNativePath    () ;

       ///
       /// \brief render the path
       ///
       void
       Render           (sf::RenderTarget& target, sf::Renderer& renderer) const;
   private:
        VGPath          m_Path;

        sf::ResourcePtr<Paint>
                        m_Stroke,
                        m_Fill;

        float           m_StrokeWidth;

        /*
        sf::Vector2f    m_Position;
        sf::Vector2f    m_Scale;
        float           m_Rotation;
        */
};
#endif
