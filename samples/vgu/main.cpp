
#include <ctype.h>
#include <math.h>
#include <iostream>
#include <jpeglib.h>
#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>
#include <zin/vg/Path.hpp>

#include "Converter.h"


#include <zin/vg/Color.hpp>
#include <zin/vg/Gradient.hpp>


int main(int argc, char **argv)
{
    int wx=450, wy=450;
    sf::RenderWindow win(sf::VideoMode(wx,wy,32), "TEST SF VG", sf::Style::Default, sf::ContextSettings(24,
                                                                                    8,
                                                                                    8));
        win.SetFramerateLimit(50);
    sf::Event event;
    sf::View view = win.GetDefaultView();
    win.SetView(view);

    if(!vgCreateContextSH(wx,wy)) // Create VG context based on the openGL context
        return EXIT_FAILURE;

    vgSetf(VG_STROKE_LINE_WIDTH, 1.f);

    VGfloat black[] = {1,1,1,1};
    VGfloat white[] = {1,1,1,1};

#define NUM_PRIMITIVES 9
    Path* primitives[NUM_PRIMITIVES];


///////
//CREATE PRIMITIVES
///////

    VGfloat points[] = {-30,-30, 30,-30, 0,30};

    Color col(sf::Color(255,0,0));
    Color col2(sf::Color(0,0,255));

    LinearGradient grad(sf::Vector2f(0.f,0.f), sf::Vector2f(1.f,0.f));
        grad.SetStop(0.f, sf::Color(123,46,237));
        grad.SetStop(0.5f, sf::Color(111, 189, 67));
        grad.SetStop(1.f, sf::Color(111, 189, 237));
        grad.SetSpread(Gradient::Pad);

        VGfloat stops[] = {
            0.0, 1.0, 0.0, 0.0, 1,
            0.5, 0.0, 1.0, 0.0, 1,
            1.0, 0.0, 0.0, 1.0, 1};

        VGint numstops = sizeof(stops) / sizeof(VGfloat);
            VGfloat radial[5];
            radial[0] = 0.5f;
            radial[1] = 0.5f;
            radial[2] = 1.f;
            radial[3] = 1.f;
            radial[4] = 1.f;


    VGPaint gradi = vgCreatePaint();
        vgSetParameteri(gradi, VG_PAINT_COLOR_RAMP_SPREAD_MODE, VG_COLOR_RAMP_SPREAD_REPEAT);
        vgSetParameteri(gradi, VG_PAINT_TYPE, VG_PAINT_TYPE_RADIAL_GRADIENT);
        vgSetParameterfv(gradi, VG_PAINT_RADIAL_GRADIENT, 5, radial);
        vgSetParameterfv(gradi, VG_PAINT_COLOR_RAMP_STOPS, numstops, stops);


    Path line;
    line.SetFill(grad);
    vguLine(line.GetNativePath(), -30,-30,30,30);

    Path polyOpen;
    polyOpen.SetFill(col);
    polyOpen.SetStroke(col2);
    vguPolygon(polyOpen.GetNativePath(), points, 3, VG_FALSE);

    Path polyClosed;
    polyClosed.SetFill(grad);
    vguPolygon(polyClosed.GetNativePath(), points, 3, VG_TRUE);

    Path rect;
    vguRect(rect.GetNativePath(), -50,-30, 100,60);
    rect.SetFill(grad);

    VGPath pathtest = vgCreatePath(VG_PATH_FORMAT_STANDARD,    // path format
                          VG_PATH_DATATYPE_F,         // datatype
                          1,                          // scale
                          0,                          // bias,
                          0,                          // segCapacityHint
                          0,                          // coord capacity hint
                          VG_PATH_CAPABILITY_ALL );   // capabilities
    vguRect(pathtest, -50, -30, 100, 60);


    Path rectRound;
    rectRound.SetFill(grad);
    vguRoundRect(rectRound.GetNativePath(), -50,-30, 100, 60, 10,10);

    Path ellipse;
    vguEllipse(ellipse.GetNativePath(), 0,0,100, 60);

    Path arcOpen;
    vguArc(arcOpen.GetNativePath(), 0,0, 100,60, 0, 270, VGU_ARC_OPEN);

    Path arcChord;
    vguArc(arcChord.GetNativePath(), 0,0, 100,60, 0, 270, VGU_ARC_CHORD);

    Path arcPie;
    vguArc(arcPie.GetNativePath(), 0,0, 100,60, 0, 270, VGU_ARC_PIE);


    primitives[0] = &line;
    primitives[1] = &polyOpen;
    primitives[2] = &polyClosed;
    primitives[3] = &rect;
    primitives[4] = &rectRound;
    primitives[5] = &ellipse;
    primitives[6] = &arcOpen;
    primitives[7] = &arcChord;
    primitives[8] = &arcPie;


    float offx = wx / 4;
    float offy = wy / 4;


    line.SetPosition        ( sf::Vector2f(offx   , offy) );
    polyOpen.SetPosition    ( sf::Vector2f(offx*2 , offy ));
    polyClosed.SetPosition  ( sf::Vector2f(offx*3 , offy) );

    rect.SetPosition  ( sf::Vector2f(offx   , offy*2) );
    rectRound.SetPosition   ( sf::Vector2f(offx*2 , offy*2) );
    ellipse.SetPosition     ( sf::Vector2f(offx*3 , offy*2) );

    arcOpen.SetPosition     ( sf::Vector2f(offx   , offy*3) );
    arcChord.SetPosition    ( sf::Vector2f(offx*2 , offy*3) );
    arcPie.SetPosition      ( sf::Vector2f(offx*3 , offy*3) );


    const sf::Input& input = win.GetInput();

    float rot = 0.f;

    sf::Text fps;
       fps.SetColor(sf::Color::Black);
    while(win.IsOpened()) {

        fps.SetString( Converter::str(1/win.GetFrameTime()) );

        while(win.GetEvent(event)) {
            if(event.Type == sf::Event::Closed)
                win.Close();
            else if(event.Type == sf::Event::MouseMoved) {
                //        drag(event.MouseMove.X, event.MouseMove.Y);
            }
            else if(event.Type == sf::Event::MouseButtonPressed) {
                //          click(0,event.MouseButton.X,event.MouseButton.Y);
            }
            else if(event.Type == sf::Event::MouseButtonReleased) {
                //            click(1,event.MouseButton.X,event.MouseButton.Y);
            }
            else if(event.Type == sf::Event::TextEntered) {
                //      key((const char)event.Text.Unicode, input.GetMouseX(), input.GetMouseY());
            }
            else if(event.Type == sf::Event::Resized) {

                wx = event.Size.Width;
                wy = event.Size.Height;

                vgResizeSurfaceSH(wx, wy);
                view.SetSize(wx, wy);
            }
        }


        //win.Clear(sf::Color::White);
        win.SaveGLStates();

        vgSetfv(VG_CLEAR_COLOR, 4, white);
        vgClear(0, 0, wx, wy);
        //win.SetActive(true);
        //win.Draw(fps);
        //win.SetActive(false);
        rot += win.GetFrameTime()*30;

        VGErrorCode err = vgGetError();
        if(err)
            std::cout << " ERR : " << err << std::endl;
        Path* primitive=primitives[0];
        for (int i=0; i <= NUM_PRIMITIVES; i++) {
            win.Draw(*primitive);
            primitive->SetRotation(rot);
            primitive=primitives[i];
        }

        vgSeti(VG_MATRIX_MODE, VG_MATRIX_FILL_PAINT_TO_USER);
        vgLoadIdentity();
        vgTranslate(40.f, 50.f);
        vgRotate(20.f);
        vgScale(2.f,2.f);

        vgSeti(VG_MATRIX_MODE, VG_MATRIX_PATH_USER_TO_SURFACE);
        vgLoadIdentity();
        vgSetPaint(gradi, VG_FILL_PATH );
        vgTranslate(40.f, 50.f);
        vgDrawPath(pathtest, VG_FILL_PATH | VG_STROKE_PATH);

        win.RestoreGLStates();
        win.Display();
    }


    return EXIT_SUCCESS;
}
