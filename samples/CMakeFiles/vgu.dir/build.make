# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 2.8

#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canoncical targets will work.
.SUFFIXES:

# Remove some rules from gmake that .SUFFIXES does not remove.
SUFFIXES =

.SUFFIXES: .hpux_make_needs_suffix_list

# Suppress display of executed commands.
$(VERBOSE).SILENT:

# A target that is always out of date.
cmake_force:
.PHONY : cmake_force

#=============================================================================
# Set environment variables for the build.

# The shell in which to execute make rules.
SHELL = /bin/sh

# The CMake executable.
CMAKE_COMMAND = /usr/bin/cmake

# The command to remove a file.
RM = /usr/bin/cmake -E remove -f

# The program to use to edit the cache.
CMAKE_EDIT_COMMAND = /usr/bin/ccmake

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = /home/alexandre/projects/sfvg

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = /home/alexandre/projects/sfvg

# Include any dependencies generated for this target.
include samples/CMakeFiles/vgu.dir/depend.make

# Include the progress variables for this target.
include samples/CMakeFiles/vgu.dir/progress.make

# Include the compile flags for this target's objects.
include samples/CMakeFiles/vgu.dir/flags.make

samples/CMakeFiles/vgu.dir/vgu/main.cpp.o: samples/CMakeFiles/vgu.dir/flags.make
samples/CMakeFiles/vgu.dir/vgu/main.cpp.o: samples/vgu/main.cpp
	$(CMAKE_COMMAND) -E cmake_progress_report /home/alexandre/projects/sfvg/CMakeFiles $(CMAKE_PROGRESS_1)
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Building CXX object samples/CMakeFiles/vgu.dir/vgu/main.cpp.o"
	cd /home/alexandre/projects/sfvg/samples && /usr/bin/c++   $(CXX_DEFINES) $(CXX_FLAGS) -o CMakeFiles/vgu.dir/vgu/main.cpp.o -c /home/alexandre/projects/sfvg/samples/vgu/main.cpp

samples/CMakeFiles/vgu.dir/vgu/main.cpp.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/vgu.dir/vgu/main.cpp.i"
	cd /home/alexandre/projects/sfvg/samples && /usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -E /home/alexandre/projects/sfvg/samples/vgu/main.cpp > CMakeFiles/vgu.dir/vgu/main.cpp.i

samples/CMakeFiles/vgu.dir/vgu/main.cpp.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/vgu.dir/vgu/main.cpp.s"
	cd /home/alexandre/projects/sfvg/samples && /usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -S /home/alexandre/projects/sfvg/samples/vgu/main.cpp -o CMakeFiles/vgu.dir/vgu/main.cpp.s

samples/CMakeFiles/vgu.dir/vgu/main.cpp.o.requires:
.PHONY : samples/CMakeFiles/vgu.dir/vgu/main.cpp.o.requires

samples/CMakeFiles/vgu.dir/vgu/main.cpp.o.provides: samples/CMakeFiles/vgu.dir/vgu/main.cpp.o.requires
	$(MAKE) -f samples/CMakeFiles/vgu.dir/build.make samples/CMakeFiles/vgu.dir/vgu/main.cpp.o.provides.build
.PHONY : samples/CMakeFiles/vgu.dir/vgu/main.cpp.o.provides

samples/CMakeFiles/vgu.dir/vgu/main.cpp.o.provides.build: samples/CMakeFiles/vgu.dir/vgu/main.cpp.o
.PHONY : samples/CMakeFiles/vgu.dir/vgu/main.cpp.o.provides.build

# Object files for target vgu
vgu_OBJECTS = \
"CMakeFiles/vgu.dir/vgu/main.cpp.o"

# External object files for target vgu
vgu_EXTERNAL_OBJECTS =

bin/vgu: samples/CMakeFiles/vgu.dir/vgu/main.cpp.o
bin/vgu: lib/libzin-zinc.so
bin/vgu: /usr/local/lib/libsfml-graphics.so
bin/vgu: /usr/local/lib/libsfml-window.so
bin/vgu: /usr/local/lib/libsfml-system.so
bin/vgu: samples/CMakeFiles/vgu.dir/build.make
bin/vgu: samples/CMakeFiles/vgu.dir/link.txt
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --red --bold "Linking CXX executable ../bin/vgu"
	cd /home/alexandre/projects/sfvg/samples && $(CMAKE_COMMAND) -E cmake_link_script CMakeFiles/vgu.dir/link.txt --verbose=$(VERBOSE)

# Rule to build all files generated by this target.
samples/CMakeFiles/vgu.dir/build: bin/vgu
.PHONY : samples/CMakeFiles/vgu.dir/build

samples/CMakeFiles/vgu.dir/requires: samples/CMakeFiles/vgu.dir/vgu/main.cpp.o.requires
.PHONY : samples/CMakeFiles/vgu.dir/requires

samples/CMakeFiles/vgu.dir/clean:
	cd /home/alexandre/projects/sfvg/samples && $(CMAKE_COMMAND) -P CMakeFiles/vgu.dir/cmake_clean.cmake
.PHONY : samples/CMakeFiles/vgu.dir/clean

samples/CMakeFiles/vgu.dir/depend:
	cd /home/alexandre/projects/sfvg && $(CMAKE_COMMAND) -E cmake_depends "Unix Makefiles" /home/alexandre/projects/sfvg /home/alexandre/projects/sfvg/samples /home/alexandre/projects/sfvg /home/alexandre/projects/sfvg/samples /home/alexandre/projects/sfvg/samples/CMakeFiles/vgu.dir/DependInfo.cmake --color=$(COLOR)
.PHONY : samples/CMakeFiles/vgu.dir/depend

