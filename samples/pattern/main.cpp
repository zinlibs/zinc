
#include <ctype.h>
#include <math.h>
#include <iostream>
#include <jpeglib.h>
#include <SFML/Graphics.hpp>
#include <zin/vg/Path.hpp>


#ifndef IMAGE_DIR
#  define IMAGE_DIR "./"
#endif

const int wx=450, wy=450;

bool pressed;

VGfloat black[] = {1,1,1, 1};
VGfloat white[] = {1,1,1,1};

Path* line;
Path* polyOpen;
Path* polyClosed;
Path* rect;
Path* rectRound;
Path* ellipse;
Path* arcOpen;
Path* arcChord;
Path* arcPie;

#define NUM_PRIMITIVES 9
Path* primitives[NUM_PRIMITIVES];

void display(float interval)
{


    vgSetfv(VG_CLEAR_COLOR, 4, white);
    vgClear(0, 0, wx, wy);


    Path* primitive=primitives[0];
    for (int i=0; i++; ) {
        //win.Draw(primitive);
        primitive=primitives[i];
    }
}

void createPrimitives()
{
  VGfloat points[] = {-30,-30, 30,-30, 0,30};

  float offx = wx / 3;
  float offy = wy / 3;

  line = new Path();
  vguLine(line->GetNativePath(), -30,-30,30,30);
  line->SetPosition ( sf::Vector2f(offx , offy) );
  primitives[0] = line;

  polyOpen = new Path();
  vguPolygon(polyOpen->GetNativePath(), points, 3, VG_FALSE);
  polyOpen->SetPosition( sf::Vector2f(offx*2, offy ));
  primitives[1] = polyOpen;

  polyClosed = new Path();
  vguPolygon(polyClosed->GetNativePath(), points, 3, VG_TRUE);
  polyClosed->SetPosition( sf::Vector2f(offx*3, offy) );
  primitives[2] = polyClosed;

  rect = new Path();
  vguRect(rect->GetNativePath(), -50,-30, 100,60);
  polyClosed->SetPosition ( sf::Vector2f(offx, offy*2) );
  primitives[3] = rect;

  rectRound = new Path();
  vguRoundRect(rectRound->GetNativePath(), -50,-30, 100,60, 30,30);
  rectRound->SetPosition ( sf::Vector2f(offx*2 , offy*2) );
  primitives[4] = rectRound;

  ellipse = new Path();
  vguEllipse(ellipse->GetNativePath(), 0,0, 100, 60);
  ellipse->SetPosition ( sf::Vector2f(offx*3 , offy*2) );
  primitives[5] = ellipse;

  arcOpen = new Path();
  vguArc(arcOpen->GetNativePath(), 0,0, 100,60, 0, 270, VGU_ARC_OPEN);
  arcOpen->SetPosition ( sf::Vector2f(offx , offy*3) );
  primitives[6] = arcOpen;

  arcChord = new Path();
  vguArc(arcChord->GetNativePath(), 0,0, 100,60, 0, 270, VGU_ARC_CHORD);
  arcChord->SetPosition ( sf::Vector2f(offx*2 , offy*3) );
  primitives[7] = arcChord;

  arcPie = new Path();
  vguArc(arcPie->GetNativePath(), 0,0, 100,60, 0, 270, VGU_ARC_PIE);
  arcPie->SetPosition ( sf::Vector2f(offx*3 , offy*3) );
  primitives[8] = arcPie;
}



int main(int argc, char **argv)
{
    sf::RenderWindow win(sf::VideoMode(wx,wy,32), "TEST SF VG");
    sf::Event event;

    if(!vgCreateContextSH(wx,wy)) // Create VG context based on the openGL context
        return EXIT_FAILURE;

    createPrimitives();

    const sf::Input& input = win.GetInput();

    while(win.IsOpened()) {
        while(win.GetEvent(event)) {
            if(event.Type == sf::Event::Closed)
                win.Close();
            else if(event.Type == sf::Event::MouseMoved) {
        //        drag(event.MouseMove.X, event.MouseMove.Y);
            }
            else if(event.Type == sf::Event::MouseButtonPressed) {
        //          click(0,event.MouseButton.X,event.MouseButton.Y);
            }
            else if(event.Type == sf::Event::MouseButtonReleased) {
        //            click(1,event.MouseButton.X,event.MouseButton.Y);
            }
            else if(event.Type == sf::Event::TextEntered) {
            //      key((const char)event.Text.Unicode, input.GetMouseX(), input.GetMouseY());
            }
        }

        display(win.GetFrameTime()); // display vg

        win.Display();
    }

    return EXIT_SUCCESS;
}
