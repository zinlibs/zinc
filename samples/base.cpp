#include <SFML/Graphics.hpp>
#include <iostream>
#include <vg/openvg.h>
#include <zin/vg/Path.hpp>

int main () {

    sf::RenderWindow window(sf::VideoMode(800,600,32), "SF VG");
    sf::Event event;


    if (!vgCreateContextSH(800,600))
        std::cout << "erreur" << std::endl ;


    VGfloat white[] {1.f,1.f,1.f,1.f};

    Path path;
    vguRect(path.GetNativePath(), 100.f,100.f ,600.f,400.f );


   VGfloat red[]   {1,0,0,1};


    while(window.IsOpened()) {
        while(window.GetEvent(event)) {
            if( event.Type == sf::Event::Closed )
               window.Close();
        }

        vgSeti(VG_MATRIX_MODE, VG_MATRIX_PATH_USER_TO_SURFACE);
        vgSetfv(VG_CLEAR_COLOR, 4,white);
        vgClear(0,0,800,600);
        path.Render();

        window.Display();

    }


    return EXIT_SUCCESS;
}
